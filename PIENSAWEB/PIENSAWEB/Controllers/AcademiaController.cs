﻿using CrystalDecisions.CrystalReports.Engine;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace PIENSAWEB.Controllers
{
    public class AcademiaController : Controller
    {

        private piensaEntities db = new piensaEntities();

        // GET: Academia
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Prematricula()
        {
            return View();
        }
       
        public ActionResult preguntas()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }

        public ActionResult Preinscripcioncurso()
        {
            ViewBag.telefonia = new SelectList(db.Telefonia, "IdTelefonia", "Nombre");
            ViewBag.cursos = new SelectList(db.CursosLibres.Where(w=>w.TipoCurso==4 && w.reganulado==false), "IdCurso", "Curso");
            return View();
        }

        public ActionResult Preinscripciondiplomado()
        {
            ViewBag.telefonia = new SelectList(db.Telefonia, "IdTelefonia", "Nombre");
            ViewBag.cursos = new SelectList(db.CursosLibres.Where(w => w.TipoCurso == 5 && w.reganulado == false), "IdCurso", "Curso");
            return View();
        }
        public ActionResult Preinscripcionespecialidad()
        {
            ViewBag.telefonia = new SelectList(db.Telefonia, "IdTelefonia", "Nombre");
            ViewBag.cursos = new SelectList(db.CursosLibres.Where(w => w.TipoCurso == 8 && w.reganulado == false), "IdCurso", "Curso");
            return View();
        }
        public ActionResult Preinscripcionseminario()
        {
            ViewBag.telefonia = new SelectList(db.Telefonia, "IdTelefonia", "Nombre");
            ViewBag.cursos = new SelectList(db.CursosLibres.Where(w => w.reganulado == false), "IdCurso", "Curso");
            return View();
        }

        public ActionResult Maestrias()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
        public ActionResult Especialidades()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
        public ActionResult CursosLibres()
        {


            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w =>w.Controlador==controlador && w.Vista==vista).ToList();

            return View(pag);
        }
        public ActionResult Diplomados()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }

        [HttpPost]
        public ActionResult preguntasend(Contacto ct, string Token)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    if(IsReCaptchValid(Token)) { 
                        string sIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                   
                        ct.asunto = "Pregunta Maestria";                    
                        ct.ip = sIP;
                        ct.FechaHoraCreado = DateTime.Now;
                        db.Contacto.Add(ct);
                        db.SaveChanges();
                  

                        fn_CredencialesEmail_Result fn = db.fn_CredencialesEmail().FirstOrDefault();

                   

                        SmtpClient smtpClient = new SmtpClient(fn.HOST, fn.PORT);

                        smtpClient.Credentials = new System.Net.NetworkCredential(fn.USERCREDENTIAL, fn.PASSCREDENTIAL);
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtpClient.EnableSsl = false;
                        MailMessage mail = new MailMessage();

                        //Setting From , To and CC
                        mail.From = new MailAddress(ct.Correo, "PIENSA SITIO WEB");
                        mail.To.Add(new MailAddress("luz.molina@piensa.uni.edu.ni"));
                        mail.Bcc.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));
                        mail.CC.Add(new MailAddress("luzvio05@yahoo.com"));
                    
                        mail.Subject = "Pregunta de Maestría";
                        mail.SubjectEncoding = System.Text.Encoding.UTF8;
                        mail.Body = "<table style='border: 1pt outset gray; height: 307px;' width='100%'><tbody><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'>Notificaci&oacute;n de Consulta por parte de usuarios en la Web</span><br /></span></p></td></tr><tr style='height: 285px;'><td style='padding: 3.75pt; width: 570px; height: 285px;'><p style='text-align: right;'><span style='font-size: 10pt;'><span style='font-family: arial, helvetica, sans-serif;'><strong>"+ct.FechaHoraCreado+"</strong></span>&nbsp;</span></p><table style='width: 100%;' border='1'><tbody><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong>Nombre</strong></span></td><td style='width: 81.2281%;'>"+ct.NombresApellidos+"</td></tr><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong>Correo</strong></span></td><td style='width: 81.2281%;'>"+ct.Correo+"</td></tr><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong>Pregunta</strong></span></td><td style='width: 81.2281%;'><em><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'>\""+ct.Mensaje+ "\"</span></em></td></tr></tbody></table><p>&nbsp;</p><p><span style='font-size: 10pt; font-family: arial, helvetica, sans-serif;'><strong>La informaci&oacute;n descrita fue enviada por un usuario desde el sitio web <a title='FAQ Piensa' href='https://piensa.uni.edu.ni/academia/preguntas' target='_blank'>Preguntas Frecuentes Maestr&iacute;as - PIENSA</a></strong></span></p></td></tr><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'>Divisi&oacute;n de Tecnolog&iacute;as de la Informaci&oacute;n y Comunicaci&oacute;n.</span><br /></span></p></td></tr></tbody></table>";
                        mail.BodyEncoding = System.Text.Encoding.UTF8;
                        mail.IsBodyHtml = true;

                        smtpClient.Send(mail);

                        dbContextTransaction.Commit();
                        return Json(new
                        {
                            message = "exito",

                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            message = "error",
                            error = "Error con el captcha"
                        });
                    }
                }
                catch (Exception er)
                {

                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message + er.InnerException + er.Data.ToString()
                    });
                }
            }




        }

        public bool IsReCaptchValid(string token)
        {
            var result = false;
            var captchaResponse = token;
            var secretKey = "6LfWcRYUAAAAAGDFwSVSOZms2aLAZim_4WAYcVZv";
            var apiUrl = "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}";
            var requestUri = string.Format(apiUrl, secretKey, captchaResponse);
            var request = (HttpWebRequest)WebRequest.Create(requestUri);

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    JObject jResponse = JObject.Parse(stream.ReadToEnd());
                    var isSuccess = jResponse.Value<bool>("success");
                    result = (isSuccess) ? true : false;
                }
            }
            return result;
        }


        [HttpPost]
        public ActionResult saveprematricula(Prematricula prematricula, string Token)
        {
            using ( var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (IsReCaptchValid(Token))
                    {
                        string sIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        prematricula.FechaHora = DateTime.Now;
                        prematricula.ip = sIP;
                        db.Prematricula.Add(prematricula);
                        db.SaveChanges();

                        dbContextTransaction.Commit();
                        WebClient myClient = new WebClient();
                        //byte[] bytes = myClient.DownloadData("");
                        fn_CredencialesEmail_Result fn = db.fn_CredencialesEmail().FirstOrDefault();

                        

                            SmtpClient smtpClient = new SmtpClient(fn.HOST, fn.PORT);

                        smtpClient.Credentials = new System.Net.NetworkCredential(fn.USERCREDENTIAL, fn.PASSCREDENTIAL);
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtpClient.EnableSsl = false;
                        MailMessage mail = new MailMessage();

                        //Setting From , To and CC
                        mail.From = new MailAddress("noreply@piensa.uni.edu.ni", "PIENSA SITIO WEB");
                        //mail.To.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));
                        mail.To.Add(new MailAddress("luz.molina@piensa.uni.edu.ni"));
                        mail.CC.Add(new MailAddress("luzvio05@yahoo.com"));
                        mail.Bcc.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));

                        mail.Subject = "Preinscripcion Maestría";
                        mail.SubjectEncoding = System.Text.Encoding.UTF8;
                        mail.Body = "<table style='border: 1pt outset gray; height: 307px;' width='100%'><tbody><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'> Notificaci&oacute;n de preinscripci&oacute;n de maestr&iacute;a PIENSA</span><br /></span></p></td></tr><tr style='height: 285px;'><td style='padding: 3.75pt; width: 570px; height: 285px;'><p style='text-align: right;'><span style='font-size: 10pt;'><span style='font-family: arial, helvetica, sans-serif;'><strong> "+prematricula.FechaHora+" </strong></span>&nbsp;</span></p><table style='width: 100%;' border='1'><tbody><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong> Nombre </strong></span></td><td style='width: 81.2281%;'> "+prematricula.NombresApellidos+" </td></tr><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong> Correo </strong></span></td><td style='width: 81.2281%;'> "+prematricula.Correo+" </td></tr></tbody></table><p><span style='font-size: 10pt; font-family: arial, helvetica, sans-serif;'><strong> La informaci&oacute;n descrita fue enviada por un usuario desde el sitio web <a title='FAQ Piensa' href='https://piensa.uni.edu.ni/academia/prematricula' target='_blank'> Prematriculas - PIENSA.</ a> Se adjunta archivo PDF de la preinscripci&oacute;n.Tambien puede revisar la preinscripci&oacute;n en el sitio https://piensa.uni.edu.ni/academia/prematriculas</strong></span></p><p>&nbsp;</p></td></tr><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'>Divisi&oacute;n de Tecnolog&iacute;as de la Informaci&oacute;n y Comunicaci&oacute;n.</span><br /></span></p></td></tr></tbody></table>";
                        mail.BodyEncoding = System.Text.Encoding.UTF8;
                        mail.IsBodyHtml = true;
                        var url = "https://www.piensa.uni.edu.ni/Academia/PExport?pre=" + prematricula.IdPrematricula;
                        byte[] bytes = myClient.DownloadData(url);
                        Stream stream = new MemoryStream(bytes);
                        Attachment attachment = new Attachment(stream, "Prematricula no-"+prematricula.IdPrematricula+".pdf", MediaTypeNames.Application.Pdf);
                        mail.Attachments.Add(attachment);                      

                        smtpClient.Send(mail);


                       
                        return Json(new
                        {
                            message = "exito",

                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            message = "error",
                            error = "Error con Captcha de Google. Vuelva a intentarlo."
                        });
                    }
                }
                catch (Exception er)
                {

                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }
            }




        }



        [HttpPost]
        public ActionResult saveprecurso(PreinscripcionCursos pre)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    string sIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    pre.FechaHora = DateTime.Now;
                    pre.ip = sIP;                             
                    db.PreinscripcionCursos.Add(pre);
                    db.SaveChanges();
                    dbContextTransaction.Commit();
                    WebClient myClient = new WebClient();
                    //byte[] bytes = myClient.DownloadData("");
                    fn_CredencialesEmail_Result fn = db.fn_CredencialesEmail().FirstOrDefault();



                    SmtpClient smtpClient = new SmtpClient(fn.HOST, fn.PORT);

                    smtpClient.Credentials = new System.Net.NetworkCredential(fn.USERCREDENTIAL, fn.PASSCREDENTIAL);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = false;
                    MailMessage mail = new MailMessage();

                    //Setting From , To and CC
                    mail.From = new MailAddress("noreply@piensa.uni.edu.ni", "PIENSA SITIO WEB");
                    //mail.To.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));
                    mail.To.Add(new MailAddress("luz.molina@piensa.uni.edu.ni"));
                    mail.CC.Add(new MailAddress("luzvio05@yahoo.com"));
                    mail.Bcc.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));

                    mail.Subject = "Preinscripcion Curso de posgrado";
                    mail.SubjectEncoding = System.Text.Encoding.UTF8;
                    mail.Body = "<table style='border: 1pt outset gray; height: 307px;' width='100%'><tbody><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'> Notificaci&oacute;n de preinscripci&oacute;n de curso de posgrado PIENSA</span><br /></span></p></td></tr><tr style='height: 285px;'><td style='padding: 3.75pt; width: 570px; height: 285px;'><p style='text-align: right;'><span style='font-size: 10pt;'><span style='font-family: arial, helvetica, sans-serif;'><strong> " + pre.FechaHora + " </strong></span>&nbsp;</span></p><table style='width: 100%;' border='1'><tbody><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong> Nombre </strong></span></td><td style='width: 81.2281%;'> " + pre.Nombres+" "+pre.Apellidos + " </td></tr><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong> Correo </strong></span></td><td style='width: 81.2281%;'> " + pre.EmailPersonal + " </td></tr></tbody></table><p><span style='font-size: 10pt; font-family: arial, helvetica, sans-serif;'><strong> La informaci&oacute;n descrita fue enviada por un usuario desde el sitio web <a title='FAQ Piensa' href='https://piensa.uni.edu.ni/academia/preinscripcioncurso' target='_blank'> Prematriculas - PIENSA.</ a> Se adjunta archivo PDF de la preinscripci&oacute;n.Tambien puede revisar la preinscripci&oacute;n en el sitio https://piensa.uni.edu.ni/academia/listacursoslibres </strong></span></p><p>&nbsp;</p></td></tr><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'>Divisi&oacute;n de Tecnolog&iacute;as de la Informaci&oacute;n y Comunicaci&oacute;n.</span><br /></span></p></td></tr></tbody></table>";
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    mail.IsBodyHtml = true;
                    var url = "https://www.piensa.uni.edu.ni/Academia/PExportc?pre=" + pre.IdPreinscripcion;
                    byte[] bytes = myClient.DownloadData(url);
                    Stream stream = new MemoryStream(bytes);
                    Attachment attachment = new Attachment(stream, "Prematricula  no-" + pre.IdPreinscripcion + ".pdf", MediaTypeNames.Application.Pdf);
                    mail.Attachments.Add(attachment);

                    smtpClient.Send(mail);
                    return Json(new
                    {
                        message = "exito",

                    });
                }
                catch (Exception er)
                {
                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }


            }


        }


        [HttpPost]
        public ActionResult saveprediplomados(PreinscripcionCursos pre)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    string sIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    pre.FechaHora = DateTime.Now;
                    pre.ip = sIP;
                    db.PreinscripcionCursos.Add(pre);
                    db.SaveChanges();
                    dbContextTransaction.Commit();
                    WebClient myClient = new WebClient();
                    //byte[] bytes = myClient.DownloadData("");
                    fn_CredencialesEmail_Result fn = db.fn_CredencialesEmail().FirstOrDefault();



                    SmtpClient smtpClient = new SmtpClient(fn.HOST, fn.PORT);

                    smtpClient.Credentials = new System.Net.NetworkCredential(fn.USERCREDENTIAL, fn.PASSCREDENTIAL);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = false;
                    MailMessage mail = new MailMessage();

                    //Setting From , To and CC
                    mail.From = new MailAddress("noreply@piensa.uni.edu.ni", "PIENSA SITIO WEB");
                    //mail.To.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));
                    mail.To.Add(new MailAddress("luz.molina@piensa.uni.edu.ni"));
                    mail.CC.Add(new MailAddress("luzvio05@yahoo.com"));
                    mail.Bcc.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));

                    mail.Subject = "Preinscripcion de Diplomado";
                    mail.SubjectEncoding = System.Text.Encoding.UTF8;
                    mail.Body = "<table style='border: 1pt outset gray; height: 307px;' width='100%'><tbody><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'> Notificaci&oacute;n de preinscripci&oacute;n de Diplomado PIENSA</span><br /></span></p></td></tr><tr style='height: 285px;'><td style='padding: 3.75pt; width: 570px; height: 285px;'><p style='text-align: right;'><span style='font-size: 10pt;'><span style='font-family: arial, helvetica, sans-serif;'><strong> " + pre.FechaHora + " </strong></span>&nbsp;</span></p><table style='width: 100%;' border='1'><tbody><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong> Nombre </strong></span></td><td style='width: 81.2281%;'> " + pre.Nombres + " " + pre.Apellidos + " </td></tr><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong> Correo </strong></span></td><td style='width: 81.2281%;'> " + pre.EmailPersonal + " </td></tr></tbody></table><p><span style='font-size: 10pt; font-family: arial, helvetica, sans-serif;'><strong> La informaci&oacute;n descrita fue enviada por un usuario desde el sitio web <a title='FAQ Piensa' href='https://piensa.uni.edu.ni/academia/preinscripciondiplomado' target='_blank'> Prematriculas - PIENSA.</ a> Se adjunta archivo PDF de la preinscripci&oacute;n.Tambien puede revisar la preinscripci&oacute;n en el sitio https://piensa.uni.edu.ni/academia/listadiplomados </strong></span></p><p>&nbsp;</p></td></tr><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'>Divisi&oacute;n de Tecnolog&iacute;as de la Informaci&oacute;n y Comunicaci&oacute;n.</span><br /></span></p></td></tr></tbody></table>";
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    mail.IsBodyHtml = true;
                    var url = "https://www.piensa.uni.edu.ni/Academia/PExportd?pre=" + pre.IdPreinscripcion;
                    byte[] bytes = myClient.DownloadData(url);
                    Stream stream = new MemoryStream(bytes);
                    Attachment attachment = new Attachment(stream, "Prematricula no-" + pre.IdPreinscripcion + ".pdf", MediaTypeNames.Application.Pdf);
                    mail.Attachments.Add(attachment);

                    smtpClient.Send(mail);
                    return Json(new
                    {
                        message = "exito",

                    });
                }
                catch (Exception er)
                {
                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }


            }


        }

        [HttpPost]
        public ActionResult savepreespecialidades(PreinscripcionCursos pre)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    string sIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    pre.FechaHora = DateTime.Now;
                    pre.ip = sIP;
                    db.PreinscripcionCursos.Add(pre);
                    db.SaveChanges();
                    dbContextTransaction.Commit();
                    WebClient myClient = new WebClient();
                    //byte[] bytes = myClient.DownloadData("");
                    fn_CredencialesEmail_Result fn = db.fn_CredencialesEmail().FirstOrDefault();


                    SmtpClient smtpClient = new SmtpClient(fn.HOST, fn.PORT);

                    smtpClient.Credentials = new System.Net.NetworkCredential(fn.USERCREDENTIAL, fn.PASSCREDENTIAL);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = false;
                    MailMessage mail = new MailMessage();

                    //Setting From , To and CC
                    mail.From = new MailAddress("noreply@piensa.uni.edu.ni", "PIENSA SITIO WEB");
                    //mail.To.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));
                    mail.To.Add(new MailAddress("luz.molina@piensa.uni.edu.ni"));
                    mail.CC.Add(new MailAddress("luzvio05@yahoo.com"));
                    mail.Bcc.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));

                    mail.Subject = "Preinscripcion de Especialidad";
                    mail.SubjectEncoding = System.Text.Encoding.UTF8;
                    mail.Body = "<table style='border: 1pt outset gray; height: 307px;' width='100%'><tbody><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'> Notificaci&oacute;n de preinscripci&oacute;n de Especialidad PIENSA</span><br /></span></p></td></tr><tr style='height: 285px;'><td style='padding: 3.75pt; width: 570px; height: 285px;'><p style='text-align: right;'><span style='font-size: 10pt;'><span style='font-family: arial, helvetica, sans-serif;'><strong> " + pre.FechaHora + " </strong></span>&nbsp;</span></p><table style='width: 100%;' border='1'><tbody><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong> Nombre </strong></span></td><td style='width: 81.2281%;'> " + pre.Nombres + " " + pre.Apellidos + " </td></tr><tr><td style='width: 18.7719%;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><strong> Correo </strong></span></td><td style='width: 81.2281%;'> " + pre.EmailPersonal + " </td></tr></tbody></table><p><span style='font-size: 10pt; font-family: arial, helvetica, sans-serif;'><strong> La informaci&oacute;n descrita fue enviada por un usuario desde el sitio web <a title='FAQ Piensa' href='https://piensa.uni.edu.ni/academia/preinscripcionespecialidad' target='_blank'> Prematriculas - PIENSA.</ a> Se adjunta archivo PDF de la preinscripci&oacute;n.Tambien puede revisar la preinscripci&oacute;n en el sitio https://piensa.uni.edu.ni/academia/listaespecialidades </strong></span></p><p>&nbsp;</p></td></tr><tr style='height: 22.5pt;'><td style='border: 0pt inset black; background: #0d47a1 none repeat scroll 0% 0%; padding: 3.75pt; height: 22px; width: 570px;'><p style='text-align: center;'><span style='font-family: arial, helvetica, sans-serif; font-size: 12pt;'><span style='color: #ffffff;'>Divisi&oacute;n de Tecnolog&iacute;as de la Informaci&oacute;n y Comunicaci&oacute;n.</span><br /></span></p></td></tr></tbody></table>";
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    mail.IsBodyHtml = true;
                    var url = "https://www.piensa.uni.edu.ni/Academia/PExporte?pre=" + pre.IdPreinscripcion;
                    byte[] bytes = myClient.DownloadData(url);
                    Stream stream = new MemoryStream(bytes);
                    Attachment attachment = new Attachment(stream, "Prematricula no-" + pre.IdPreinscripcion + ".pdf", MediaTypeNames.Application.Pdf);
                    mail.Attachments.Add(attachment);

                    smtpClient.Send(mail);
                    return Json(new
                    {
                        message = "exito",

                    });
                }
                catch (Exception er)
                {
                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }


            }


        }
        [Authorize]
        public ActionResult Prematriculas()
        {
            return View();
        }

        [Authorize]
        public ActionResult Listacursoslibres()
        {
            return View();
        }

        [Authorize]
        public ActionResult Listadiplomados()
        {
            return View();
        }

        [Authorize]
        public ActionResult Listaespecialidades()
        {
            return View();
        }


        [Authorize]
        public ActionResult searchPrematriculas()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (db)
            {
                var v = (from a in db.Prematricula select new {a.IdPrematricula,Mencion = a.MencionesMaestria.Nombre, a.NombresApellidos,a.Cedula,a.Nacionalidad,a.DireccionDomiciliar,a.NumeroCasa,a.NumeroClaro,a.NumeroMovistar,a.NumeroTrabajo,a.Correo,a.CentrodeTrabajo,a.PuestoTrabajo,a.TipoApoyoEmpleador,a.PadeceEnfermedades,a.NumeroContacto,a.TituloAcadamico,a.Universidad,a.PromediodeNotas,Lectura = a.Habilidad.Habilidad1,Escritura = a.Habilidad1.Habilidad1,Conversacion= a.Habilidad2.Habilidad1,a.FormadeCulminacion,a.ExperenciaTrabajosCienciasAmbientales,a.Motivaciones,a.IdeadeTesina,a.FechaHora,a.NombreApellidosContacto
                });


                if (!(string.IsNullOrEmpty(searchv)))
                {
                    v = v.Where(a =>

                        a.Mencion.Contains(searchv) ||
                         a.NombresApellidos.Contains(searchv) 
                        );
                }
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    v = v.OrderBy(sortColumn + " " + sortColumnDir);
                }
                totalRecords = v.Count();
                var data = v.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public ActionResult searchPrematriculasc()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (db)
            {
                var c = (from a in db.PreinscripcionCursos where a.CursosLibres.TipoCurso ==4
                         select new 
                         {
                             a.IdPreinscripcion,
                             NombresApellidos= a.Nombres+" "+a.Apellidos,  
                             a.CursosLibres.Curso,                          
                             a.Cedula,
                             a.Ciudad,
                             a.DireccionDomiciliar,
                             a.TelefonoConvencional,
                             a.TelefonoTrabajo,
                             a.Movil,
                             Telefonia = a.Telefonia1.Nombre,
                             a.EmailOficina,
                             a.EmailPersonal,
                             a.PuestoTrabajo,
                             a.Emergencia,
                             a.NumeroEmergencia,
                             a.Profesion,
                             a.Titulo,
                             a.Organizacion,
                             a.DireccionTrabajo,                            
                             a.FuturusCursos,
                             a.FechaHora,
                             a.ip
                         });

                var v = (from a in c
                         select new
                         {
                             a.IdPreinscripcion,
                             a.Curso,
                             a.NombresApellidos,
                             a.Cedula,
                             a.Ciudad,
                             a.DireccionDomiciliar,
                             a.TelefonoConvencional,
                             a.TelefonoTrabajo,
                             a.Movil,
                             a.Telefonia,
                             a.EmailOficina,
                             a.EmailPersonal,
                             a.PuestoTrabajo,
                             a.Emergencia,
                             a.NumeroEmergencia,
                             a.Profesion,
                             a.Titulo,
                             a.Organizacion,
                             a.DireccionTrabajo,
                             a.FuturusCursos,
                             FechaHora = a.FechaHora.ToString(),
                             a.ip

                         });


                if (!(string.IsNullOrEmpty(searchv)))
                {
                    v = v.Where(a =>
                        
                        a.Cedula.Contains(searchv) ||
                         a.EmailOficina.Contains(searchv) ||
                          a.EmailPersonal.Contains(searchv) ||
                           a.Movil.ToString().Contains(searchv) ||
                         a.NombresApellidos.Contains(searchv)
                        );
                }
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    v = v.OrderBy(sortColumn + " " + sortColumnDir);
                }
                totalRecords = v.Count();
                var data = v.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public ActionResult searchPrematriculasd()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (db)
            {
                var c = (from a in db.PreinscripcionCursos
                         where a.CursosLibres.TipoCurso == 5
                         select new
                         {
                             a.IdPreinscripcion,
                             NombresApellidos = a.Nombres + " " + a.Apellidos,
                             a.CursosLibres.Curso,
                             a.Cedula,
                             a.Ciudad,
                             a.DireccionDomiciliar,
                             a.TelefonoConvencional,
                             a.TelefonoTrabajo,
                             a.Movil,
                             Telefonia = a.Telefonia1.Nombre,
                             a.EmailOficina,
                             a.EmailPersonal,
                             a.PuestoTrabajo,
                             a.Emergencia,
                             a.NumeroEmergencia,
                             a.Profesion,
                             a.Titulo,
                             a.Organizacion,
                             a.DireccionTrabajo,
                             a.FuturusCursos,
                             a.FechaHora,
                             a.ip
                         });

                var v = (from a in c
                         select new
                         {
                             a.IdPreinscripcion,
                             a.Curso,
                             a.NombresApellidos,
                             a.Cedula,
                             a.Ciudad,
                             a.DireccionDomiciliar,
                             a.TelefonoConvencional,
                             a.TelefonoTrabajo,
                             a.Movil,
                             a.Telefonia,
                             a.EmailOficina,
                             a.EmailPersonal,
                             a.PuestoTrabajo,
                             a.Emergencia,
                             a.NumeroEmergencia,
                             a.Profesion,
                             a.Titulo,
                             a.Organizacion,
                             a.DireccionTrabajo,
                             a.FuturusCursos,
                             FechaHora = a.FechaHora.ToString(),
                             a.ip

                         });


                if (!(string.IsNullOrEmpty(searchv)))
                {
                    v = v.Where(a =>

                        a.Cedula.Contains(searchv) ||
                         a.EmailOficina.Contains(searchv) ||
                          a.EmailPersonal.Contains(searchv) ||
                           a.Movil.ToString().Contains(searchv) ||
                         a.NombresApellidos.Contains(searchv)
                        );
                }
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    v = v.OrderBy(sortColumn + " " + sortColumnDir);
                }
                totalRecords = v.Count();
                var data = v.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }
        [Authorize]
        public ActionResult searchPrematriculase()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (db)
            {
                var c = (from a in db.PreinscripcionCursos
                         where a.CursosLibres.TipoCurso == 8
                         select new
                         {
                             a.IdPreinscripcion,
                             NombresApellidos = a.Nombres + " " + a.Apellidos,
                             a.CursosLibres.Curso,
                             a.Cedula,
                             a.Ciudad,
                             a.DireccionDomiciliar,
                             a.TelefonoConvencional,
                             a.TelefonoTrabajo,
                             a.Movil,
                             Telefonia = a.Telefonia1.Nombre,
                             a.EmailOficina,
                             a.EmailPersonal,
                             a.PuestoTrabajo,
                             a.Emergencia,
                             a.NumeroEmergencia,
                             a.Profesion,
                             a.Titulo,
                             a.Organizacion,
                             a.DireccionTrabajo,
                             a.FuturusCursos,
                             a.FechaHora,
                             a.ip
                         });

                var v = (from a in c
                         select new
                         {
                             a.IdPreinscripcion,
                             a.Curso,
                             a.NombresApellidos,
                             a.Cedula,
                             a.Ciudad,
                             a.DireccionDomiciliar,
                             a.TelefonoConvencional,
                             a.TelefonoTrabajo,
                             a.Movil,
                             a.Telefonia,
                             a.EmailOficina,
                             a.EmailPersonal,
                             a.PuestoTrabajo,
                             a.Emergencia,
                             a.NumeroEmergencia,
                             a.Profesion,
                             a.Titulo,
                             a.Organizacion,
                             a.DireccionTrabajo,
                             a.FuturusCursos,
                             FechaHora = a.FechaHora.ToString(),
                             a.ip

                         });


                if (!(string.IsNullOrEmpty(searchv)))
                {
                    v = v.Where(a =>

                        a.Cedula.Contains(searchv) ||
                         a.EmailOficina.Contains(searchv) ||
                          a.EmailPersonal.Contains(searchv) ||
                           a.Movil.ToString().Contains(searchv) ||
                         a.NombresApellidos.Contains(searchv)
                        );
                }
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    v = v.OrderBy(sortColumn + " " + sortColumnDir);
                }
                totalRecords = v.Count();
                var data = v.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public ActionResult Reportepreinscripcionmaestria(string preinscripcion)
        {
            
                ReportViewerViewModel model = new ReportViewerViewModel();

                var user = User.Identity.GetUserName();
                var iduser = User.Identity.GetUserId();

                string content = Url.Content("~/Reportes/ASP/CRV.aspx?id=1&prematricula=" + preinscripcion + "&user=" + user);
                model.ReportPath = content;



                return View("ReportePreinscripcionMaestria", model);

            


        }

       
        public ActionResult PExport(string pre)
        {
           
            ReportDocument reportDocument = new ReportDocument();
            reportDocument.Load(Server.MapPath("~/Reportes/ReportePrema.rpt"));
            reportDocument.SetDatabaseLogon("piensauser", "Lvca46YHcSPxA93X", "192.168.103.74", "piensa");
            reportDocument.SetParameterValue("@idprema", pre);
                      Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream st = reportDocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            st.Seek(0, SeekOrigin.Begin);
            return File(st, "application/pdf", "Prematricula n-" + pre + ".pdf");

        }

       

        public ActionResult PExportc(string pre)
        {

            ReportDocument reportDocument = new ReportDocument();
            reportDocument.Load(Server.MapPath("~/Reportes/ReportePremaCursos.rpt"));
            reportDocument.SetDatabaseLogon("piensauser", "Lvca46YHcSPxA93X", "192.168.103.74", "piensa");
            reportDocument.SetParameterValue("@idprema", pre);           
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream st = reportDocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            st.Seek(0, SeekOrigin.Begin);
            return File(st, "application/pdf", "Prematricula Cursos n-" + pre+".pdf");

        }

        public ActionResult PExporte(string pre)
        {

            ReportDocument reportDocument = new ReportDocument();
            reportDocument.Load(Server.MapPath("~/Reportes/ReportePremaEspecialidades.rpt"));
            reportDocument.SetDatabaseLogon("piensauser", "Lvca46YHcSPxA93X", "192.168.103.74", "piensa");
            reportDocument.SetParameterValue("@idprema", pre);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream st = reportDocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            st.Seek(0, SeekOrigin.Begin);
            return File(st, "application/pdf", "Prematricula Especialidades n-" + pre + ".pdf");

        }


        public ActionResult PExportd(string pre)
        {

            ReportDocument reportDocument = new ReportDocument();
            reportDocument.Load(Server.MapPath("~/Reportes/ReportePremaDiplomados.rpt"));
            reportDocument.SetDatabaseLogon("piensauser", "Lvca46YHcSPxA93X", "192.168.103.74", "piensa");
            reportDocument.SetParameterValue("@idprema", pre);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream st = reportDocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            st.Seek(0, SeekOrigin.Begin);
            return File(st, "application/pdf", "Prematricula Diplomados n-" + pre + ".pdf");

        }


        [Authorize]
        public ActionResult searchPrecursos()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (db)
            {
                var v = (from a in db.PreinscripcionCursos
                         select new
                         {
                             a.IdPreinscripcion,
                             a.Nombres,
                             a.Apellidos,
                             a.Cedula,
                             a.Ciudad,
                             a.DireccionDomiciliar,
                             a.DireccionTrabajo,
                             a.EmailOficina,
                             a.EmailPersonal,
                             a.Emergencia,
                             a.FechaHora,
                             a.FuturusCursos,
                             a.ip,
                             a.Movil,
                             a.NumeroEmergencia,
                             a.Organizacion,
                             a.Profesion,
                             a.PuestoTrabajo,
                             Telefonia = a.Telefonia1.Nombre,
                             a.TelefonoConvencional,
                             a.TelefonoTrabajo,
                             a.Titulo,
                             a.CursosLibres.Curso                           
                         });


                if (!(string.IsNullOrEmpty(searchv)))
                {
                    v = v.Where(a =>

                        a.Nombres.Contains(searchv) ||
                         a.Apellidos.Contains(searchv) ||
                          a.Cedula.Contains(searchv) ||
                           a.EmailPersonal.Contains(searchv)
                        );
                }
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    v = v.OrderBy(sortColumn + " " + sortColumnDir);
                }
                totalRecords = v.Count();
                var data = v.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public ActionResult Reportepreinscripciocursos(string preinscripcion)
        {

            ReportViewerViewModel model = new ReportViewerViewModel();

            var user = User.Identity.GetUserName();
            var iduser = User.Identity.GetUserId();

            string content = Url.Content("~/Reportes/ASP/CRV.aspx?id=2&prematricula=" + preinscripcion + "&user=" + user);
            model.ReportPath = content;



            return View("ReportePreinscripcionMaestria", model);




        }

        
        
    }
}