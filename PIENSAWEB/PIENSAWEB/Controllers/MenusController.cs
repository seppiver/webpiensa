﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;


using PIENSAWEB.Models;

namespace PIENSAWEB.Controllers
{
    public class MenusController : Controller
    {
        private piensaEntities db = new piensaEntities();

        // GET: Menus
        [Authorize]
        public ActionResult Index()
        {
            var menu = db.Menu.Include(m => m.Menu2).Include(m => m.Pagina).Include(m => m.Users);
            return View(menu.ToList());
        }
        [Authorize]
        // GET: Menus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menu.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }
            return View(menu);
        }
        [Authorize]
        // GET: Menus/Create
        public ActionResult Create()
        {
            ViewBag.MenuPadre = new SelectList(db.Menu, "IdMenu", "Menu1");
            ViewBag.IdPagina = new SelectList(db.Pagina, "idPagina", "Vista");
            ViewBag.UsuarioCreacion = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: Menus/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "IdMenu,Menu1,MenuPadre,Icono,Orden,IdPagina,Habilitar,VisibleCMS")] Menu menu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    menu.UsuarioCreacion = HttpContext.User.Identity.GetUserId();
                    menu.FechaCreacion = DateTime.Now;

                    db.Menu.Add(menu);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.MenuPadre = new SelectList(db.Menu, "IdMenu", "Menu1", menu.MenuPadre);
                ViewBag.IdPagina = new SelectList(db.Pagina, "idPagina", "Vista", menu.IdPagina);
                ViewBag.UsuarioCreacion = new SelectList(db.Users, "Id", "Email", menu.UsuarioCreacion);
                return View(menu);
            }
            catch(Exception er)
            {
                ViewBag.MenuPadre = new SelectList(db.Menu, "IdMenu", "Menu1", menu.MenuPadre);
                ViewBag.IdPagina = new SelectList(db.Pagina, "idPagina", "Vista", menu.IdPagina);
                ViewBag.UsuarioCreacion = new SelectList(db.Users, "Id", "Email", menu.UsuarioCreacion);
                return View(menu);

            }
        }
        [Authorize]
        // GET: Menus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menu.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }
            ViewBag.MenuPadre = new SelectList(db.Menu, "IdMenu", "Menu1", menu.MenuPadre);
            ViewBag.IdPagina = new SelectList(db.Pagina, "idPagina", "Vista", menu.IdPagina);
            ViewBag.UsuarioCreacion = new SelectList(db.Users, "Id", "Email", menu.UsuarioCreacion);
            return View(menu);
        }

        // POST: Menus/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "IdMenu,Menu1,MenuPadre,Icono,Orden,FechaCreacion,FechaAnulacion,FechaModificacion,UsuarioCreacion,IdPagina,Habilitar,VisibleCMS")] Menu menu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(menu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MenuPadre = new SelectList(db.Menu, "IdMenu", "Menu1", menu.MenuPadre);
            ViewBag.IdPagina = new SelectList(db.Pagina, "idPagina", "Vista", menu.IdPagina);
            ViewBag.UsuarioCreacion = new SelectList(db.Users, "Id", "Email", menu.UsuarioCreacion);
            return View(menu);
        }

        // GET: Menus/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menu.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }
            return View(menu);
        }

        // POST: Menus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Menu menu = db.Menu.Find(id);
            db.Menu.Remove(menu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}
