﻿using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIENSAWEB.Controllers
{
    public class LaboratoriosController : Controller
    {
        private piensaEntities db = new piensaEntities();
       

        public ActionResult FisicoQuimico()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
        public ActionResult AguasResiduales()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
        public ActionResult Microbiologia()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
        public ActionResult Micropoluentes()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
        public ActionResult Aire()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }

        public ActionResult side()
        {
            
                var pag = db.Pagina.Where(w => w.Nombre == "Laboratorios").ToList();
                return PartialView(pag);
            
        }
    }
}