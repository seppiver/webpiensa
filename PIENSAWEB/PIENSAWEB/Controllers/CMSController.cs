﻿using PIENSAWEB.Models;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net;
using System.IO;
using PIENSAWEB.Clases;
using System.Drawing;
using System.Text;
using Microsoft.AspNet.Identity;

namespace PIENSAWEB.Controllers
{
    [Authorize]
    public class CMSController : Controller
    {
        private piensaEntities db = new piensaEntities();

        // GET: CMS
        public ActionResult Noticias()
        {
            return View();
        }

        public ActionResult index()
        {
            return View();
        }
        public ActionResult Paginas()
        {
            return View();
        }

        public ActionResult imagenes()
        {
            return View();
        }
        public ActionResult Eventos()
        {
            return View();
        }

        public ActionResult Menus()
        {
           
            return View();
        }

        public ActionResult CreateFotos()
        {
            return PartialView();
        }
        public ActionResult CreateNoticias()
        {
            return PartialView();
        }
        public ActionResult CreateEventos()
        {
            return PartialView();
        }
        public ActionResult CreatePaginas()
        {
            return PartialView();
        }
        public ActionResult CreateDocumentos()
        {
            return PartialView();
        }
        public ActionResult Createdoc()
        {
            return PartialView();
        }

        public ActionResult Documentos()
        {
            return View();
        }

        public ActionResult CreateMenu()
        {
           
            return PartialView();
        }

        [HttpPost]
        public ActionResult upphoto(HttpPostedFileBase photo)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {

                    
                    Archivos p = new Archivos();
                    p.FechaHoraCreacion = DateTime.Now;
                    p.Nombre = photo.FileName;
                    p.IsFoto = true;
                    p.UsuarioCreacion = HttpContext.User.Identity.GetUserId();
                    db.Archivos.Add(p);
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "photo");


                    var fileName = Path.GetFileName(photo.FileName);
                    var path = Path.Combine(Server.MapPath("~/img/uploads"), fileName);
                    photo.SaveAs(path);

                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }
           

        }

        [HttpPost]
        public ActionResult EliminarPhoto(int id)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    Archivos p = db.Archivos.Where(w => w.IdArchivo == id).FirstOrDefault();
                    db.Archivos.Remove(p);
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "photo");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }


        [HttpPost]
        public ActionResult updoc(HttpPostedFileBase doc)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {


                    Archivos p = new Archivos();
                    p.FechaHoraCreacion = DateTime.Now;
                    p.Nombre = doc.FileName;
                    p.IsFoto = false;
                    p.UsuarioCreacion = HttpContext.User.Identity.GetUserId();
                    db.Archivos.Add(p);
                    
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "photo");

                    var fileName = Path.GetFileName(doc.FileName);
                    var path = Path.Combine(Server.MapPath("~/doc/uploads"), fileName);
                    doc.SaveAs(path);

                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }


        }

        [HttpPost]
        public ActionResult EliminarDoc(int id)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    Archivos p = db.Archivos.Where(w => w.IdArchivo == id).FirstOrDefault();
                    db.Archivos.Remove(p);
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "photo");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }

        [HttpPost,ValidateInput(false)]
        public ActionResult agregarnoticia(Post p)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    //p.Contenido = HttpUtility.HtmlEncode(p.Contenido);
                    p.reganulado = false;
                    p.FechaHora = DateTime.Now;
                    p.UsuarioCreacion = HttpContext.User.Identity.GetUserId();
                    db.Post.Add(p);
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "post");
                    dbContextTransaction.Commit();

                    return Json(new
                    {
                        message = "exito",

                    });
                }
                catch (Exception er)
                {

                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }


            }
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult Editarnoticia(Post p)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    p.FechaHora = db.Post.Where(w => w.Idpost == p.Idpost).Select(s => s.FechaHora).FirstOrDefault();
                    p.reganulado = false;
                    db.Entry(p).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "post");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }


        [HttpPost]
        public ActionResult EliminarNoticia(int id)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    Post r = db.Post.Where(w => w.Idpost == id).FirstOrDefault();
                    r.reganulado = true;
                    db.Entry(r).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "post");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }


        
        [HttpPost,ValidateInput(false)]
        public ActionResult agregarevento(Post p)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    //p.Contenido = HttpUtility.HtmlEncode(p.Contenido);
                    p.reganulado = false;
                    p.FechaHora = DateTime.Now;
                    p.UsuarioCreacion = HttpContext.User.Identity.GetUserId();
                    db.Post.Add(p);
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "post");
                    dbContextTransaction.Commit();
                    return Json(new
                    {
                        message = "exito",

                    });
                }
                catch (Exception er)
                {

                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }


            }
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult Editarevento(Post p)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    p.FechaHora = db.Post.Where(w => w.Idpost == p.Idpost).Select(s => s.FechaHora).FirstOrDefault();
                    p.reganulado = false;
                    db.Entry(p).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "post");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }


        [HttpPost]
        public ActionResult Eliminarevento(int id)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    Post r = db.Post.Where(w => w.Idpost == id).FirstOrDefault();
                    r.reganulado = true;
                    db.Entry(r).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "post");
                    dbContextTransaction.Commit();
                   
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }


        [HttpPost, ValidateInput(false)]
        public ActionResult Editarpagina(Pagina p)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                   
                    db.Entry(p).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "pagina");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }




        public ActionResult searchNoticias()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (piensaEntities b = new piensaEntities())
            {
                var v = (from a in b.Post.Where(w => w.tipo == 1 && w.reganulado == false) select a);
                v.OrderBy("FechaHora asc");
                var d = (from a in v select new { a.Idpost,a.reganulado,a.tipo,a.Titulo,a.Contenido, FechaHora= a.FechaHora.ToString(), a.Imagen });



                if (!(string.IsNullOrEmpty(searchv)))
                {
                    d = d.Where(a =>
                        a.Contenido.ToString().Contains(searchv) ||
                        a.FechaHora.Contains(searchv) ||
                        a.Titulo.ToString().Contains(searchv) 
                        


                        );
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    d = d.OrderBy(sortColumn + " " + sortColumnDir);
                }


                totalRecords = d.Count();
                var data = d.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }


        public ActionResult searchEventos()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (piensaEntities b = new piensaEntities())
            {
                var v = (from a in b.Post.Where(w => w.tipo == 2 && w.reganulado == false) select a);
                v.OrderBy("FechaHora asc");
                var d = (from a in v select new { a.Idpost,fechafin= a.FechaHoraFinEvento,fechainicio= a.FechaHoraInicioEvento, a.reganulado, a.tipo, a.Titulo, a.Contenido, FechaHora = a.FechaHora });



                if (!(string.IsNullOrEmpty(searchv)))
                {
                    d = d.Where(a =>
                        a.Contenido.ToString().Contains(searchv) ||
                        a.FechaHora.ToString().Contains(searchv) ||
                        a.Titulo.ToString().Contains(searchv)



                        );
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    d = d.OrderBy(sortColumn + " " + sortColumnDir);
                }


                totalRecords = d.Count();
                var data = d.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }
        public ActionResult searchImagenes()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (piensaEntities b = new piensaEntities())
            {
                var v = (from a in b.Archivos where a.IsFoto == true select a);
                v.OrderBy("FechaHoraCreacion asc");
                var d = (from a in v select new { a.IdArchivo, a.Nombre,FechaHoraCreacion = a.FechaHoraCreacion.ToString(), url = "../img/uploads/"+a.Nombre });



                if (!(string.IsNullOrEmpty(searchv)))
                {
                    d = d.Where(a =>
                        a.Nombre.ToString().Contains(searchv) ||
                        a.FechaHoraCreacion.Contains(searchv) 
                        



                        );
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    d = d.OrderBy(sortColumn + " " + sortColumnDir);
                }


                totalRecords = d.Count();
                var data = d.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult searchDocumentos()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (piensaEntities b = new piensaEntities())
            {
                var v = (from a in b.Archivos where a.IsFoto==false select a );
                v.OrderBy("FechaHoraCreacion asc");
                var d = (from a in v select new { a.IdArchivo, a.Nombre, FechaHoraCreacion = a.FechaHoraCreacion.ToString(), url = "../doc/uploads/" + a.Nombre });



                if (!(string.IsNullOrEmpty(searchv)))
                {
                    d = d.Where(a =>
                        a.Nombre.ToString().Contains(searchv) ||
                        a.FechaHoraCreacion.Contains(searchv)




                        );
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    d = d.OrderBy(sortColumn + " " + sortColumnDir);
                }


                totalRecords = d.Count();
                var data = d.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult searchPaginas()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (piensaEntities b = new piensaEntities())
            {
                var v = (from a in b.Pagina select a);
                var d = (from a in v where a.FechaEliminacion==null select new { a.idPagina, a.Nombre, a.Contenido,a.Controlador,a.Vista, url = 
                         a.VisibleCMS==true ? "pagina/publicacion?name="+a.Nombre : "" + a.Controlador + "/" + a.Vista
                         ,
                    Editable = a.VisibleCMS });



                if (!(string.IsNullOrEmpty(searchv)))
                {
                    d = d.Where(a =>
                        a.Contenido.ToString().Contains(searchv) 
                      


                        );
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    d = d.OrderBy(sortColumn + " " + sortColumnDir);
                }


                totalRecords = d.Count();
                var data = d.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult agregarPagina(Pagina p)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    //p.Contenido = HttpUtility.HtmlEncode(p.Contenido);
                    p.FechaEliminacion = null;
                    p.FechaCreacion = DateTime.Now;
                    p.VisibleCMS = true;
                    p.UsuarioCreacion = HttpContext.User.Identity.GetUserId();
                    db.Pagina.Add(p);
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "pagina");
                    dbContextTransaction.Commit();

                    return Json(new
                    {
                        message = "S",

                    });
                }
                catch (Exception er)
                {

                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }


            }
        }


        [HttpPost]
        public ActionResult EliminarPagina(int id)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    Pagina r = db.Pagina.Where(w => w.idPagina == id).FirstOrDefault();
                    r.FechaEliminacion = DateTime.Now;
                    db.Entry(r).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "pagina");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }


        [HttpPost, ValidateInput(false)]
        public ActionResult agregarmenu(Menu p)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    //p.Contenido = HttpUtility.HtmlEncode(p.Contenido);
                    if(p.IdPagina == 0)
                    {
                        p.IdPagina = null;
                    }
                    if (p.MenuPadre == 0)
                    {
                        p.MenuPadre = null;
                    }
                    p.FechaAnulacion = null;
                    p.FechaModificacion = null;
                    p.FechaCreacion = DateTime.Now;
                    p.Habilitar = true;
                    p.VisibleCMS = true;
                    p.UsuarioCreacion = HttpContext.User.Identity.GetUserId();
                    db.Menu.Add(p);
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "Menu");
                    dbContextTransaction.Commit();

                    return Json(new
                    {
                        message = "S",

                    });
                }
                catch (Exception er)
                {

                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }


            }
        }


        [HttpPost]
        public ActionResult Eliminarmenu(int id)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    Menu r = db.Menu.Where(w => w.IdMenu == id).FirstOrDefault();
                    r.FechaAnulacion = DateTime.Now;
                    db.Entry(r).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "menu");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }



        [HttpPost, ValidateInput(false)]
        public ActionResult Editarmenu(Menu p)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    Menu original = db.Menu.Where(w => w.IdMenu == p.IdMenu).FirstOrDefault();

                    original.Menu1 = p.Menu1;
                    original.MenuPadre = p.MenuPadre;
                    original.Orden = p.Orden;
                    original.Icono = p.Icono;
                    original.IdPagina = p.IdPagina;
                    db.Entry(original).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Sp_Audit_Table_User(User.Identity.GetUserId(), "menu");
                    dbContextTransaction.Commit();
                    return Json(new { Message = "S" });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Message = "Error" });
                }
            }

        }



        public ActionResult searchMenu()
        {

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var lenght = Request.Form.GetValues("length").FirstOrDefault();

            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var searchv = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pagesize = lenght != null ? Convert.ToInt32(lenght) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;


            using (piensaEntities b = new piensaEntities())
            {
                var d = b.Menu.Where(w=>w.FechaAnulacion==null).Select(a=> new
                    {
                    a.IdMenu,
                            Menu = a.Menu1,
                            IdMenuPadre = a.MenuPadre,
                             MenuPadre = a.Menu2.Menu1,
                            a.Orden,
                             a.Icono,
                             Pagina = a.Pagina.Nombre,
                             a.IdPagina,
                             Editable = a.VisibleCMS
                }
                    );



                if (!(string.IsNullOrEmpty(searchv)))
                {
                    d = d.Where(a =>
                        a.Menu.ToString().Contains(searchv) 



                        );
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    d = d.OrderBy(sortColumn + " " + sortColumnDir);
                }


                totalRecords = d.Count();
                var data = d.Skip(skip).Take(pagesize).ToList();

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

            }
        }


        public ActionResult menuspadres()
        {
            var data = db.Menu.Where(w => w.FechaAnulacion == null).Select(s=>new { s.IdMenu, Menu = s.Menu1});

            return Json(new {
                data
            });
        }

        public ActionResult listapaginas()
        {
            var data = db.Pagina.Where(w => w.FechaEliminacion == null).Select(s=>new { s.idPagina, pagina = s.Nombre});

            return Json(new {
                data
            });
        }
    }
}