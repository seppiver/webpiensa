﻿using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIENSAWEB.Views
{
    public class InvestigacionController : Controller
    {
        private piensaEntities db = new piensaEntities();

       
        public ActionResult lineas()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }

        public ActionResult proyectos()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }

        public ActionResult tesis()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }

        public ActionResult cedoc()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
    }
}