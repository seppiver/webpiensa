﻿using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Piensa.uni.edu.ni.Controllers
{
    public class HomeController : Controller
    {
        private piensaEntities db = new piensaEntities();
        public ActionResult Index()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }

     
        public ActionResult Dinamico()
        {
            return View();

        }

        //[Authorize]
        public ActionResult Contact()
        {
         //   ViewBag.UserName = HttpContext.GetOwinContext().Authentication.User.Identity.Name;
           
           
            return View();
        }

        public ActionResult somos()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }

        public ActionResult Contactenos(Boolean visible)
        {
            if (visible)
            {
                var pag = db.Pagina.Where(w => w.Nombre == "Contactenos").ToList();
                return PartialView(pag);

            }
            else
            {
                return HttpNotFound();
            }

        }





        public ActionResult historia()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
        public ActionResult mision()
        {
            String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            var pag = db.Pagina.Where(w => w.Controlador == controlador && w.Vista == vista).ToList();

            return View(pag);
        }
        public ActionResult Menu( Boolean visible)
        {
            if (visible) {
                var menu = db.Menu.Where(w=>w.Habilitar==true && w.FechaAnulacion == null).OrderBy(w=>w.Orden);
                return PartialView(menu);
            }
            else
            {
                return HttpNotFound();
            }
           
        }

        public ActionResult Roulette(Boolean visible)
        {
            if (visible)
            {
                var pag = db.Pagina.Where(w => w.Nombre == "Roulette").ToList();
                return PartialView(pag);
            }
            else
            {
                return HttpNotFound();
            }
        }
        public ActionResult Footer(Boolean visible)
        {
            if (visible)
            {
                var pag = db.Pagina.Where(w => w.Nombre == "Footer").ToList();
                return PartialView(pag);
            }
            else
            {
                return HttpNotFound();
            }
        }

       


        public ActionResult savecontact(Contacto contacto)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    string sIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    contacto.FechaHoraCreado = DateTime.Now;
                    contacto.ip = sIP;
                    db.Contacto.Add(contacto);
                    db.SaveChanges();
                    fn_CredencialesEmail_Result fn = db.fn_CredencialesEmail().FirstOrDefault();



                    SmtpClient smtpClient = new SmtpClient(fn.HOST, fn.PORT);

                    smtpClient.Credentials = new System.Net.NetworkCredential(fn.USERCREDENTIAL, fn.PASSCREDENTIAL);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = false;
                    MailMessage mail = new MailMessage();

                    //Setting From , To and CC
                    mail.From = new MailAddress(contacto.Correo, "PIENSA SITIO WEB CONTACTO");
                    mail.To.Add(new MailAddress("atencion.cliente@piensa.uni.edu.ni"));
                    mail.Bcc.Add(new MailAddress("yasser.montiel@dtic.uni.edu.ni"));

                    mail.Subject = contacto.asunto;
                    mail.SubjectEncoding = System.Text.Encoding.UTF8;
                    mail.Body = contacto.Mensaje;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;


                    smtpClient.Send(mail);
                    dbContextTransaction.Commit();
                    return Json(new
                    {
                        message = "exito",

                    });
                }
                catch (Exception er)
                {
                    dbContextTransaction.Rollback();
                    return Json(new
                    {
                        message = "error",
                        error = er.Message
                    });
                }



            }

        }

        public ActionResult ModalDelete(Boolean v)
        {
            if (v == true)
            {
                return PartialView("ModalDelete");
            }
            else
            {
                return HttpNotFound();
            }

        }


    }
}