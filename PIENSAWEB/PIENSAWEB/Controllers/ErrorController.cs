﻿using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIENSAWEB.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        // GET: Error
        private piensaEntities db = new piensaEntities();
        public ActionResult NotFound()
        {
            try
            {
                String controlador = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
                String vista = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();



                var noti = db.Pagina.Where(w => w.FechaEliminacion == null && w.Controlador == controlador && w.Vista == vista).ToList();

                if (noti.Any())
                {
                    return View("Dinamico", noti);
                }
                else
                {

                    var statusCode = (int)System.Net.HttpStatusCode.NotFound;
                    Response.StatusCode = statusCode;
                    Response.TrySkipIisCustomErrors = true;
                    HttpContext.Response.StatusCode = statusCode;
                    HttpContext.Response.TrySkipIisCustomErrors = true;
                    return View();
                }
              
            }catch(Exception er)
            {
                return View("Error");
            }
          
        }

        public ActionResult Error()
        {
            Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
            Response.TrySkipIisCustomErrors = true;
            return View();
        }
    }
}