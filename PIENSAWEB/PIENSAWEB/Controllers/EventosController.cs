﻿using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIENSAWEB.Controllers
{
    public class EventosController : Controller
    {
        private piensaEntities db = new piensaEntities();
        // GET: Eventos

        public ActionResult side()
        {

            //var noti = db.Post.Where(w => w.reganulado == false && w.tipo == 2).OrderByDescending(o => o.Idpost).ToList();
            var noti = db.Post.Where(w => w.reganulado == false && w.tipo == 2).OrderByDescending(o => o.Idpost).ToList();
            return PartialView(noti);
        }

        public ActionResult publicacion(string id)
        {
            int idn = Convert.ToInt32(id);
            var noti = db.Post.Where(w => w.reganulado == false && w.tipo == 2 && w.Idpost == idn).ToList();

            return View(noti);
        }
    }
}