﻿using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIENSAWEB.Controllers
{
    
    public class PaginaController : Controller
    {
        // GET: Error
        private piensaEntities db = new piensaEntities();

        [HttpGet]      
        public ActionResult publicacion(string name)
        {
       
            var pag = db.Pagina.Where(w => w.FechaEliminacion == null && w.Nombre==name && w.VisibleCMS== true).ToList();

            return View(pag);



        }
    }
}