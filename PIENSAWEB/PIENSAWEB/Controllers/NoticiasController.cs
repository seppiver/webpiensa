﻿using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIENSAWEB.Controllers
{
    public class NoticiasController : Controller
    {
        private piensaEntities db = new piensaEntities();

        // GET: Noticias

        public ActionResult side()
        {
            var noti = db.Post.Where(w=>w.reganulado==false && w.tipo==1).OrderByDescending(o=>o.Idpost).ToList().Take(3);
            return PartialView(noti);
        }


        public ActionResult Todas()
        {
            
            return View();
        }

        public ActionResult sideindex()
        {
            var noti = db.Post.Where(w => w.reganulado == false && w.tipo == 1).OrderByDescending(o => o.Idpost).ToList().Take(5);
            return PartialView(noti);
        }


        public ActionResult publicacionn(string id)
        {
            int idn = Convert.ToInt32(id);
            var noti = db.Post.Where(w => w.reganulado == false && w.tipo == 1 && w.Idpost == idn).ToList();
          
            return View(noti);
          

            
        }



        public ActionResult searchNoticias(string pageNumber, string pageSize)
        {

           // var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //var start = Request.Form.GetValues("pageNumber").FirstOrDefault();
            //var lenght = Request.Form.GetValues("pageSize").FirstOrDefault();
         
           
            int pagesize = pageSize != null ? Convert.ToInt32(pageSize) : 0;
            int skip = (Convert.ToInt32(pagesize) * Convert.ToInt32(pageNumber)) - Convert.ToInt32(pagesize);


            int totalRecords = 0;


            using (piensaEntities b = new piensaEntities())
            {
                var d = db.Post.Where(w => w.reganulado == false && w.tipo == 1).Select(a => new
                {
                    title = a.Titulo,
                    description = a.Contenido,
                    media = a.Imagen,
                    url = "/Noticias/publicacionn?id=" + a.Idpost,
                    fecha = a.FechaHora.Value.ToString(),
                    id= a.Idpost


                });


            


                totalRecords = d.Count();
                var data = d.OrderByDescending(o=> o.id).Skip(skip).Take(pagesize).ToList();

                return Json(new {  items = data , total = totalRecords}, JsonRequestBehavior.AllowGet);

            }
        }

    }
}