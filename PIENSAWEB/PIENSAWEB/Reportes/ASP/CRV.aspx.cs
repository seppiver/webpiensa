﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIENSAWEB.Reportes.ASP
{
    public partial class CRV : System.Web.UI.Page
    {
        CrystalDecisions.CrystalReports.Engine.ReportDocument reportDocument = null;
        string fileName;

        protected void Page_Init(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            if (this.reportDocument != null)
            {
                this.reportDocument.Close();
                this.reportDocument.Dispose();
            }


            int IdR = int.Parse(Convert.ToString(Request.QueryString["id"]));
           
            Report(IdR);


        }
        private void Report(int id)
        {
            ReportDocument reportDocument = new ReportDocument();
            string user = Convert.ToString(Request.QueryString["user"]);
            switch (id)
            {
                case 1:
                    string cotizacion = Convert.ToString(Request.QueryString["prematricula"]);
                   

                    reportDocument.Load(Server.MapPath("~/Reportes/ReportePrema.rpt"));
                    reportDocument.SetDatabaseLogon("piensauser", "Lvca46YHcSPxA93X", "192.168.103.74", "piensa");
                 
                    reportDocument.SetParameterValue("@idprema", cotizacion);
                    ListCrystalReport.ID = "Preincripcion-No" + cotizacion;
                    ListCrystalReport.Zoom(150);
                    

                    break;
               
            }

            ListCrystalReport.ReportSource = reportDocument;
            ListCrystalReport.EnableParameterPrompt = false;
            ListCrystalReport.HasToggleParameterPanelButton = false;
            ListCrystalReport.HasToggleGroupTreeButton = false;
            ListCrystalReport.DataBind();
        }

        protected void ListCrystalReport_Init(object sender, EventArgs e)
        {

        }
    }
    
}