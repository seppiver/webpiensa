function Telefono(cadena) {
    cadena = NoLetras(cadena);
    var punto = cadena.substring(cadena.length - 1, cadena.length);
    if (cadena.length > 8 || cadena.indexOf('.') != -1) {
        cadena = cadena.substring(0, cadena.length - 1);
    }

    return cadena;
}

function NoLetras(cadena) {
    if (isNaN(cadena) && cadena.length > 0) {
        cadena = cadena.substring(0, cadena.length - 1);

    }
    return cadena;

}