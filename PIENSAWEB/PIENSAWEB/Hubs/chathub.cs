﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace PIENSAWEB.Hubs
{
    public class chathub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void send(string name ,string message)
        {
            Clients.All.sendMessageTopPage(name, message);
        }
    }

}