﻿File.prototype.getBase64 = function (callback, callbackprogress, callbackerror) {
    'use strict';
    var reader = new FileReader();
    reader.onload = function (e) { callback(e.target.result); };
    reader.onprogress = function (e) {
        if (typeof (callbackprogress) === 'function') { callbackprogress(e); }
    };
    reader.onerror = function (eventError) {
        if (typeof (callbackerror) === 'function') { callbackerror(eventError); }
    };
    reader.readAsDataURL(this);
};
File.prototype.getAsText = function (callback) {
    'use strict';
    var reader = new FileReader();
    reader.onload = function (e) { callback(e.target.result); };
    reader.onerror = function (e) { callback(null); };
    reader.readAsText(this);
};
angular.element(document.head).append('<style>' +
	'.upload-box{display:table; border-radius:2px; border:2px dashed rgba(52, 73, 94, 0.4); width:100%; height:200px; min-height:0px; max-height: 960px; table-layout: fixed; background-color: #ECF0F1; }' +
	'.upload-box.dragenter{border: 2px dashed #34495E;}' +
	'.upload-box label{display:table-cell; vertical-align:middle; text-align:center; text-overflow:ellipsis; overflow:hidden; width:100%; height:100%; font-size:14px; cursor:pointer; color:rgba(52, 73, 94, 0.4); /*color: #34495E;*/ }' +
	'.upload-box.dragenter label{color: #34495E;} ' +
	'.upload-box label img{margin-top:-35px;height:40%;}' +
	'.upload-box input{display:none;}' +
	'</style>');
angular.module('ngArchivo', [])
    .directive('archivo', ['$parse', '$compile', function ($parse, $compile) {
        'use strict';
        return {
            terminal: true,
            scope: false,
            restrict: 'E',
            compile: function (tElement, tAttrs) {
                return function linked(scope, elemento, attrs) {
                    var modelo = attrs.filemodel;
                    if (modelo === null || modelo === undefined || modelo === '') { throw "ng-Archivo Directiva no posee modelo"; }
                    var template = angular.element('<div class="upload-box">' +
                        '<label for="' + modelo + '_file">' +
                            '<img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDgwLjI0NCA4MC4yNDQiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDgwLjI0NCA4MC4yNDQ7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMjU2cHgiIGhlaWdodD0iMjU2cHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zOS4xLDY5LjE1NmMxLjEwNCwwLDItMC44OTYsMi0yVjM2LjkyNGwxMC4wMjMsOS43NzNjMC4zOTEsMC4zOTEsMS4wMjcsMC41ODYsMS41MzksMC41ODZzMS4wODYtMC4xOTUsMS40NzctMC41ODYgICAgYzAuNzgxLTAuNzgxLDAuODEyLTIuMDQ3LDAuMDMxLTIuODI4TDQxLjUyLDMxLjIwNGMtMC43ODEtMC43ODEtMi4wMzktMC43ODEtMi44MiwwTDI2LjAzOCw0My44NyAgICBjLTAuNzgxLDAuNzgxLTAuNzc5LDIuMDQ3LDAuMDAyLDIuODI4czEuNzk4LDAuNzgxLDIuNTc5LDBsOC40ODEtOC43M3YyOS4xODhDMzcuMSw2OC4yNiwzNy45OTYsNjkuMTU2LDM5LjEsNjkuMTU2eiIgZmlsbD0iIzM0NDk1ZSIvPgoJCTxwYXRoIGQ9Ik0xNS4wMjgsNTcuNzc1SDI5LjZjMS4xMDQsMCwyLTAuODk2LDItMnMtMC44OTYtMi0yLTJIMTUuMDI4Yy02LjI4NiwwLTExLjAyNy00LjI5OS0xMS4wMjctMTAuMjUyICAgIGMwLTUuOTUxLDQuNzQxLTEwLjM0NCwxMS4wMjctMTAuMzQ0YzEuMTIxLDAsMi4yMjMsMC4yMTMsMy4yNzUsMC41NDFsMC4zNzcsMC4xNDFjMC41ODksMC4xODQsMS4yMzIsMC4wOTYsMS43MzgtMC4yNTggICAgYzAuNTA3LTAuMzU0LDAuODIyLTAuOTE2LDAuODUzLTEuNTMzYzAuNDc5LTkuNTEyLDguMzIyLTE2Ljk2LDE3Ljg1NC0xNi45NmM3LjUyOSwwLDE0LjI4OCw0LjcyMSwxNi44MTksMTEuNzQ2ICAgIGMwLjM0OSwwLjk2NywxLjM3MywxLjUwNiwyLjM2NywxLjI2M2MwLjAyOS0wLjAwOCwwLjEyNS0wLjAzMiwwLjE1NC0wLjA0MWMxLjI3NS0wLjM4MiwyLjU5NS0wLjc3NiwzLjkxMS0wLjc3NiAgICBjNy43NzYsMCwxMy44NjcsNS43NzEsMTMuODY3LDEzLjM4M2MwLDcuNDQ3LTYuMDkxLDEzLjA5MS0xMy44NjcsMTMuMDkxSDQ5LjZjLTEuMTA0LDAtMiwwLjg5Ni0yLDJzMC44OTYsMiwyLDJoMTIuNzc3ICAgIGMxMC4wMTksMCwxNy44NjctNy40LDE3Ljg2Ny0xNy4wOTFjMC05Ljg1NC03Ljg0OS0xNy40NzgtMTcuODY3LTE3LjQ3OGMtMS4yMTgsMC0yLjM0NCwwLjI1My0zLjM3OSwwLjUxNSAgICBjLTMuNTMxLTcuNjQyLTExLjI5NS0xMi42MzMtMTkuODczLTEyLjYzM2MtMTAuODUzLDAtMTkuOTE2LDcuOTA3LTIxLjU5OCwxOC4zODFjLTAuODIyLTAuMTM4LTEuNjU3LTAuMjAxLTIuNS0wLjIwMSAgICBDNi42LDI5LjI2OSwwLDM1LjQyNCwwLDQzLjUyMUMwLDUxLjYxOSw2LjYwMSw1Ny43NzUsMTUuMDI4LDU3Ljc3NXoiIGZpbGw9IiMzNDQ5NWUiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" />' +
                            '<br>' +
                            'Selecciona o arrastra un archivo' +
                        '</label>' +
                        '<input id="' + modelo + '_file" type="file"/>' +
                    '</div>' +
                    '<progress id="' + modelo + '_progress" value="0" max="100" style="width:100%;"></progress>');
                    var base = attrs.base64;
                    if (base === undefined || base === '') { base = false; }

                    var maxsize = attrs.filesize; // KB
                    if (maxsize === undefined || maxsize === '') { maxsize = 1024; }

                    var types = attrs.filetype;
                    if (types !== undefined && types !== '') {
                        types = types.toLowerCase().replace(/\s+/g, '');
                    }


                    $compile(template)(scope);
                    elemento.replaceWith(template);
                    var $label = template.find('label');
                    var $input = template.find('input');

                    var accept = attrs.accept;
                    if (accept !== undefined && accept !== '') {
                        $input.attr('accept', accept)
                    }

                    var alto = attrs.height;
                    if (alto !== '' && alto !== undefined) {
                        template.css({ 'height': alto });
                        $label.css({ 'height': alto });
                    }

                    $input.on('change', function (event) {
                        var eventoReal = event.originalEvent || event;
                        var file = eventoReal.target.files[0];
                        asignarScope(file);
                    });

                    template.on('dragover', function (event) {
                        event.preventDefault();
                        setActivo();
                    });
                    template.on('dragleave', function (event) {
                        event.preventDefault();
                        setInactivo();
                    });
                    template.on('drop', function (event) {
                        event.preventDefault();
                        var eventoReal = event.originalEvent || event;
                        var file = eventoReal.dataTransfer.files[0];
                        asignarScope(file);
                    });
                    function setActivo() { template.addClass('dragenter'); }
                    function setInactivo() { template.removeClass('dragenter'); }

                    function asignarScope(file) {
                        if (FileTypeAccepted(file)) {
                            if (FileSizeAccepted(file)) {
                                if (base === 'true') {
                                    file.getBase64(function (texto) {
                                        scope.$apply(function () {
                                            $parse(modelo).assign(scope, texto);
                                            document.getElementById(modelo + '_progress').value = 100;
                                        });
                                    }, function (event) {
                                        var progreso = (event.loaded / event.total) * 100;
                                        console.log("Cargado: " + event.loaded + "     TOTAL: " + event.total);
                                        document.getElementById(modelo + '_progress').value = progreso;
                                    });
                                } else {
                                    $parse(modelo).assign(scope, file);
                                }
                                setLabelText(file.name + "\n" + FileSize(file));
                            } else {
                                AlertMessage("Tamaño Maximo Excedido\n" + "Permitido: " + maxsize + "KB");
                            }
                        } else {
                            AlertMessage("Tipo de archivo no permitido\n" + "solo: " + types);
                        }
                    }

                    function setLabelText(texto) {
                        $label.text(texto);
                        $label.prepend('<br>');
                        $label.prepend('<img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDgwLjI0NCA4MC4yNDQiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDgwLjI0NCA4MC4yNDQ7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMjU2cHgiIGhlaWdodD0iMjU2cHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zOS4xLDY5LjE1NmMxLjEwNCwwLDItMC44OTYsMi0yVjM2LjkyNGwxMC4wMjMsOS43NzNjMC4zOTEsMC4zOTEsMS4wMjcsMC41ODYsMS41MzksMC41ODZzMS4wODYtMC4xOTUsMS40NzctMC41ODYgICAgYzAuNzgxLTAuNzgxLDAuODEyLTIuMDQ3LDAuMDMxLTIuODI4TDQxLjUyLDMxLjIwNGMtMC43ODEtMC43ODEtMi4wMzktMC43ODEtMi44MiwwTDI2LjAzOCw0My44NyAgICBjLTAuNzgxLDAuNzgxLTAuNzc5LDIuMDQ3LDAuMDAyLDIuODI4czEuNzk4LDAuNzgxLDIuNTc5LDBsOC40ODEtOC43M3YyOS4xODhDMzcuMSw2OC4yNiwzNy45OTYsNjkuMTU2LDM5LjEsNjkuMTU2eiIgZmlsbD0iIzM0NDk1ZSIvPgoJCTxwYXRoIGQ9Ik0xNS4wMjgsNTcuNzc1SDI5LjZjMS4xMDQsMCwyLTAuODk2LDItMnMtMC44OTYtMi0yLTJIMTUuMDI4Yy02LjI4NiwwLTExLjAyNy00LjI5OS0xMS4wMjctMTAuMjUyICAgIGMwLTUuOTUxLDQuNzQxLTEwLjM0NCwxMS4wMjctMTAuMzQ0YzEuMTIxLDAsMi4yMjMsMC4yMTMsMy4yNzUsMC41NDFsMC4zNzcsMC4xNDFjMC41ODksMC4xODQsMS4yMzIsMC4wOTYsMS43MzgtMC4yNTggICAgYzAuNTA3LTAuMzU0LDAuODIyLTAuOTE2LDAuODUzLTEuNTMzYzAuNDc5LTkuNTEyLDguMzIyLTE2Ljk2LDE3Ljg1NC0xNi45NmM3LjUyOSwwLDE0LjI4OCw0LjcyMSwxNi44MTksMTEuNzQ2ICAgIGMwLjM0OSwwLjk2NywxLjM3MywxLjUwNiwyLjM2NywxLjI2M2MwLjAyOS0wLjAwOCwwLjEyNS0wLjAzMiwwLjE1NC0wLjA0MWMxLjI3NS0wLjM4MiwyLjU5NS0wLjc3NiwzLjkxMS0wLjc3NiAgICBjNy43NzYsMCwxMy44NjcsNS43NzEsMTMuODY3LDEzLjM4M2MwLDcuNDQ3LTYuMDkxLDEzLjA5MS0xMy44NjcsMTMuMDkxSDQ5LjZjLTEuMTA0LDAtMiwwLjg5Ni0yLDJzMC44OTYsMiwyLDJoMTIuNzc3ICAgIGMxMC4wMTksMCwxNy44NjctNy40LDE3Ljg2Ny0xNy4wOTFjMC05Ljg1NC03Ljg0OS0xNy40NzgtMTcuODY3LTE3LjQ3OGMtMS4yMTgsMC0yLjM0NCwwLjI1My0zLjM3OSwwLjUxNSAgICBjLTMuNTMxLTcuNjQyLTExLjI5NS0xMi42MzMtMTkuODczLTEyLjYzM2MtMTAuODUzLDAtMTkuOTE2LDcuOTA3LTIxLjU5OCwxOC4zODFjLTAuODIyLTAuMTM4LTEuNjU3LTAuMjAxLTIuNS0wLjIwMSAgICBDNi42LDI5LjI2OSwwLDM1LjQyNCwwLDQzLjUyMUMwLDUxLjYxOSw2LjYwMSw1Ny43NzUsMTUuMDI4LDU3Ljc3NXoiIGZpbGw9IiMzNDQ5NWUiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" />');
                    }

                    function FileTypeAccepted(file) {
                        if (types === undefined || types === '') { return true; }
                        else if (types !== undefined || types !== '') {
                            var vector = types.split(',');
                            var ext = FileType(file);
                            for (var i = 0; i < vector.length; i++) {
                                if (vector[i] === ext) {
                                    return true;
                                }
                            }
                            return false;
                        }
                    }
                    function FileType(file) {
                        var name = file.name.trim();
                        var vector = name.split('.');
                        name = vector[vector.length - 1];
                        return name.toLowerCase();
                    }
                    function FileSizeAccepted(file) {
                        if ((file.size / 1024) > maxsize) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                    function FileSize(file) {
                        var tam = file.size;
                        if (tam <= 1023) {
                            tam = Math.floor(tam);
                            return '\n' + tam + ' BYTES';
                        } else if (tam >= 1024 && tam < 1048576) {
                            tam = Math.floor(tam / 1024);
                            return '\n' + tam + ' KB';
                        } else if (tam >= 1048576) {
                            tam = Math.floor(tam / 1048576);
                            return '\n' + tam + ' MB';
                        }
                    }
                    function AlertMessage(mensaje) { alert(mensaje); setInactivo(); }

                    scope.$watch(modelo, function (newvalue, oldvalue) {
                        var value = $parse(modelo)(scope);
                        if (value === undefined || value === '') {
                            setLabelText('Selecciona o arrastra un archivo');
                            setInactivo();
                            document.getElementById(modelo + '_progress').value = 0;
                        }
                    });
                };
            }
        };
    }]);