﻿using PIENSAWEB.Clases;
using PIENSAWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


[assembly: PreApplicationStartMethod(typeof(PIENSAWEB.MvcApplication), "Register")]
namespace PIENSAWEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        public static void Register()
        {
            HttpApplication.RegisterModule(typeof(QueryString));
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            string sIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
            Exception exception = Server.GetLastError();
            String url = Request.Url.AbsoluteUri;

            piensaEntities db = new piensaEntities();
            Errores le = new Errores();
            le.FechaHora = DateTime.Now;
            le.Error = exception.Message;
            le.URL = url;
            le.ip = sIP;

            db.Errores.Add(le);
            db.SaveChanges();
           

        }
    }
}
