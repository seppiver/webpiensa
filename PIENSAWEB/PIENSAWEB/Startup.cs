﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PIENSAWEB.Startup))]
namespace PIENSAWEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
